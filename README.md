# Cars API

## Run
```
mvn spring-boot:run
```
OR
```
mvn clean package
java -jar target/cars-api-0.0.1-SNAPSHOT.jar
```
## Test
```
http://localhost:8080/api/cars/JH4CU26679C029109
```