package com.cars.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Car {
	
	public Make make;
	public Model model;
	public Engine engine;
	public List<Years> years;
	public MPG MPG;
	
	public class Make {
		public String name;
	}
	
	public class Model {
		public String name;
	}
	
	public class Engine {
		public String fuelType;
	}
	
	public static class Years {
		public Integer year;
	}
	
	public class MPG {
		public String highway;
		public String city;
	}

	@Override
	public String toString() {
		try {
			if (this != null) {
				return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
			}
		} catch (JsonProcessingException e) {
		}
		
		return super.toString();
	}

}
