package com.cars.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RequestMapping("/api")
@RestController
public class CarController {
	
	@Value("${edmunds.api-url}")
	private String edmundsApiUrl;
	
	@Value("${edmunds.api-key}")
	private String edmundsApiKey;
	
	@Autowired
	private RestTemplate restTemplate;
	
	private static Logger logger = LoggerFactory.getLogger(CarController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public String ping() {
		return "Cars API";
	}
	
	@RequestMapping(value = "/cars/{vin}", method = RequestMethod.GET)
    public Car getCarInfoByVin(@PathVariable("vin") String vin) {
		
		logger.info("Get Car Info by VIN {}", vin);
		
        Car car = restTemplate.getForObject(edmundsApiUrl + "/api/vehicle/v2/vins/" + vin + "?fmt=json&api_key=" + edmundsApiKey, Car.class);
        logger.info(car.toString());
        
		return car;
    }
	
}
